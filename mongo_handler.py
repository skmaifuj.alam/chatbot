import pymongo


class SlotHandler:
    def send_slot_values(self, mylist):
        client = pymongo.MongoClient("mongodb+srv://subroto:p1@cluster0-zoou9.mongodb.net/test?retryWrites=true&w=majority")
        data = ''
        if len(mylist) == 1:
            data = client['ChatBot'][mylist[0]].find({}, {'_id': 0})[0]

        elif len(mylist) == 2:
            data = (client['ChatBot'][mylist[0]].find({mylist[1]: {'$exists': 1}}, {'_id': 0}))[0].get(mylist[1])
        print("mongo: ",data)
        return data


if __name__ == '__main__':
    ll = ['PhaseOne', 'what']
    print(SlotHandler().send_slot_values(ll))