## intent:goodBye
- bye
- bbye
- good bye
- bubye

## intent:greet
- hey
- hi
- hello
- heyya

## intent:health
- how are you?
- how is your health?
- how do you do?

## intent:phaseOne
- [tell me](what) about [Phase One](phase_type).
- [Describe](what) about [Phase One](phase_type).
- [Phase One](phase_type) [content](what).
- [Contents](what) of [Phase One](phase_type).
- [What](what) is there in [phase one](phase_type)?
- [Outcome](outcome) of [phase one](phase_type).
- [Learning](outcome) of [phase one](phase_type).
- [Assignment](assignment) of [phase_one](phase_type)
- [what](what) is in [phase one](phase_type)?
- [Details](url) of [phase one](phase_type).
- [Duration](duration) of [phase one](phase_type).
- [Time](duration) of [phase one](phase_type).

## intent:simple
- who is my mentor.
- my mentor name.

## synonym:phaseOne
- phase one
- Phase One
- phase 1
- PHASE OnE
- PhAsE OnE
