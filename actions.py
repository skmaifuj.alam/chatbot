from rasa_core_sdk import Action
from pymongo import MongoClient
import pymongo
import logging


def send_slot_values(mylist):
    client = pymongo.MongoClient("mongodb+srv://subroto:p1@cluster0-zoou9.mongodb.net/test?retryWrites=true&w=majority")
    print(client)
    data = ''
    if len(mylist) == 1:
        data = client['ChatBot'][mylist[0]].find({}, {'_id': 0})[0]
    elif len(mylist) == 2:
        data = (client['ChatBot'][mylist[0]].find({mylist[1]: {'$exists': 1}}, {'_id': 0}))[0].get(mylist[1])
    print("mongo: ", data)
    return data

class ActionPhaseOne_Default(Action):
    def name(self):
        return 'action_PhaseOne'

    def run(self, dispatcher, tracker, domain):
        cause = tracker.get_slot('cause')
        phase_type = tracker.get_slot('phase_type')
        ff=tracker.current_slot_values()
        print(ff)
        print(cause)
        print(phase_type)
        l = [cause, phase_type]
        data = send_slot_values(l)
        dispatcher.utter_message("....OK....")
        return []
