# ChatBot

RASA ChatBot

Train NLU : python3 nlu_model.py --debug

Train Core : python3 -m rasa_core.train -d domain.yml -s data/stories.md -o models/dialogue -c policy.yml

Create Endpoint on local server: python3 -m rasa_core_sdk.endpoint --actions actions

Start Project: python3 -m rasa_core.run -d models/dialogue -u models/current/nlu --endpoints endpoints.yml

kill port: sudo kill $(sudo lsof -t -i:5055)

DataBase access for Subroto: pymongo.MongoClient("mongodb+srv://subroto:p1@cluster0-zoou9.mongodb.net/test?retryWrites=true&w=majority")

Run In Debugging: python3 -m rasa_core.run -d models/dialogue -u models/current/nlu --endpoints endpoints.yml --debug

Interactive Training: python3 -m rasa_core.train interactive -o models/dialogue -d domain.yml -c policy.yml -s data/stories.md --nlu models/current/nlu --debug

python3 dialogue_model.py --endpoints endpoints.yml



